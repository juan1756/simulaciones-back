'use strict';
const AWS = require('aws-sdk');
const createError = require("http-errors");
const uuid = require("uuid");

const dynamoDBService = new AWS.DynamoDB.DocumentClient();

module.exports.hello = async (event) => {

    console.log('Event received:', event);
    let body = JSON.parse(event?.body);
    body.id = uuid.v4();

    let params = {
        TableName: process.env.MONTECARLO_TABLE,
        Item: body,
        ReturnValues: "ALL_OLD"
    };

    const response = await dynamoDBService.put(params).promise();

    if (response) {
      console.log("response: ", response)
      return {
        statusCode: 200,
        body: JSON.stringify(body, null, 2)
      };
    } else {
      throw new createError.BadRequest("Error creating user");
    }
};
